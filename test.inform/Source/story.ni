"Test" by Andre Rodriguez

Include Books by Bart Massey

The white room is a room. "This is the white room. It only holds the black book. The Minimalist Room is south from here. The Poet's bedroom is south from here.".

The black book is a book in the white room. The read text is "just a book with three pages". The title is  "The Black Book." The manuscript is the Table of Black Book Pages

Table of Black Book Pages
Page Text
"Page 1: You have found this book because you are special."
"Page 2: Is up to you what story this book might contain."
"Page 3: As you can tell this thing is misterious as heck."

The pretty bookmark is a bookmark carried by the player. The description is "Gilded flowers adorn this stylish bookmark."

The Minimalist Room is south of the white room. "This room is depressingly devoid of furniture and adornment."

[The Zorkmid coin: example of readable thing that isn't a book]
The zorkmid coin is carried by the player. The description is "This large ugly coin purports to be worth exactly one Zorkmid. Whatever that is." The read text is "One Zorkmid[paragraph break]In Focom We Trust".

The long comic book is a writable erasable book in the Minimalist Room. "[The item described] is lying on the floor, it looks like it's falling apart." The description is "A batman and robin comic book". The title is "Batman And Robin Issue #53".  The front cover text is "1956. DC Comics". The back cover text is "Sinopsis: Tim is over his head when his intentions make him refuse his allies' help in taking down the killers. Some allies believe that Tim has lost his way and needs help himself.". The read text is "This comic book only has about 6 pages in it." The manuscript is the Table of comic book pages. 

Table of comic book pages
Page Text
"Page 1: Batman punches some random person."
"Page 2: Random person turns out to be Robin."
"Page 3: Batman apologizes and takes Robin for some ice cream."
"Page 4: Robin says he didn't like the ice cream."
"Page 5: Robin takes off mask and turns out to be the Joker."
"Page 6: Batman punches the Joker right in the face."
"Page 7: The Joker says: 'Wow, that was totally uncalled for.'"

The pretty bookmark is a bookmark carried by the player. The description is "Gilded flowers adorn this stylish bookmark."


When play begins: say "The Poet's bedroom is place where vulnerability and fragile emotions are encouraged. (Once you write something in his notebook, it is there for all eternity)"

The Poet's Bedroom is north of the white room. "This is where the Poet sits down to think and reflect upon existence."

The desk is a fixed in place supporter in the Poet's Bedroom.

The Poet's Notebook is writable scenery on the desk. The description is "A notebook to transform thoughts and feelings into words. [if read text of the Poet's Notebook is not empty] [read text][end if]". Understand "notebook" as the Poet's Notebook.

To say the contents of the notebook: say "The notebook reads:"; silently try reading the notebook.

A writing tool called a pen is on the desk. The description is "A regular pen. Many stories have been written with this old friend."

White Ink is an erasing tool on the desk. The description is "Could this white ink erase something written on the notebook?"
An erasing tool called an eraser is on the desk. The description is "An eraser. Use this to erase your Poetry, but don't be too harsh on yourself, self judgement may be hurtful."
Check an actor erasing the notebook (this is the check erasing notebook rule): say "Not a chance, once you write something here there's no going back."

